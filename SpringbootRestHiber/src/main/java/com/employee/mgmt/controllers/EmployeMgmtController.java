package com.employee.mgmt.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.employee.mgmt.beans.Employee;
import com.employee.mgmt.response.CustomErrorType;

@RestController
public class EmployeMgmtController {
	
	public EmployeMgmtController() {
		System.out.println("#########EmployeMgmtController#########");
	}
	
	@GetMapping(value = "/employeemgmt/employeeinfo/{employeeId}")
	public ResponseEntity<?> getEmployeeById(@PathVariable("employeeId") Long employeeId) {
		
		Employee foundEmployee = null;
		for(Employee employee : employees) {
			if(employee.getEmployeeId().equals(employeeId)) {
				foundEmployee = employee;
				break;
			}
		}
		
		ResponseEntity<?> responseEntity = null;
		if(foundEmployee != null) {
			responseEntity = new ResponseEntity(foundEmployee, HttpStatus.OK);
		} else {
			CustomErrorType customErrorType = new CustomErrorType("Employee with Id " 
					+ employeeId + " not found.");
			responseEntity = new ResponseEntity(customErrorType, HttpStatus.NO_CONTENT);
		}
		
		return responseEntity;
	}

	@GetMapping(value = "/employeemgmt/employeeinfo/getAllEmployees")
	public ResponseEntity<List<Employee>> getEmployees() {
		
		List<Employee> employeeList = new ArrayList<Employee>();
		for(Employee emp : employees)  {
			employeeList.add(emp);
		}
		return new ResponseEntity<List<Employee>>(employeeList, HttpStatus.OK);
	}
	
	
	@PutMapping(value = "/employeemgmt/employeeinfo/saveEmployee")
	public ResponseEntity<Void> saveEmployee(@RequestBody Employee employee) {
		
		ResponseEntity<Void> responseEntity = null;
		
		if(true) {
			responseEntity = new ResponseEntity<Void>(HttpStatus.CREATED);
		} else {
			responseEntity = new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
		
		return responseEntity;
	}
	
	@PostMapping(value = "/employeemgmt/employeeinfo/updateEmployee")
	public ResponseEntity<Void> updateEmployee(@RequestBody Employee employee) {
		
		ResponseEntity<Void> responseEntity = null;
		
		if(true) {
			responseEntity = new ResponseEntity<Void>(HttpStatus.ACCEPTED);
		} else {
			responseEntity = new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
		
		return responseEntity;
	}
	
	@DeleteMapping(value = "/employeemgmt/employeeinfo/deleteEmployee")
	public ResponseEntity<Void> deleteEmployee(@PathVariable("employeeId") Long employeeId) {
		

		Employee foundEmployee = null;
		for(Employee employee : employees) {
			if(employee.getEmployeeId().equals(employeeId)) {
				foundEmployee = employee;
				break;
			}
		}
		
		ResponseEntity<Void> responseEntity = null;
		if(foundEmployee != null) {
			responseEntity = new ResponseEntity<Void>(HttpStatus.OK);
		} else {
			responseEntity = new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
		
		return responseEntity;
		
	}
	
	private static Employee[] employees = new Employee[] {
			new Employee(101L,"Farhan", "Quazi", 100000D, 8),
			new Employee(102L,"Bisweet", "Choubey", 100000D, 7),
			new Employee(103L,"Illiays", "Khan", 70000D, 5),
			new Employee(104L,"Dhiru", "dhiru", 100000D, 10),
			new Employee(105L,"Salman", "Khan", 5000000D, 15),
	};
}
