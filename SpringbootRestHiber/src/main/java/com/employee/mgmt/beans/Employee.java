package com.employee.mgmt.beans;

public class Employee {
	private Long employeeId;
	private String firstName;
	private String lastName;
	private Double ctc;
	private Integer experience;

	public Employee() {
	}

	public Employee(String firstName, String lastName, 
			Double ctc, Integer experience) {
				setCtc(ctc);
				setEmployeeId(employeeId);
				setExperience(experience);
				setFirstName(firstName);
				setLastName(lastName);
	}
	
	public Employee(Long employeeId, String firstName,
	String lastName, Double ctc, Integer experience) {
		setCtc(ctc);
		setEmployeeId(employeeId);
		setExperience(experience);
		setFirstName(firstName);
		setLastName(lastName);
	}
	
	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Double getCtc() {
		return ctc;
	}

	public void setCtc(Double ctc) {
		this.ctc = ctc;
	}

	public Integer getExperience() {
		return experience;
	}

	public void setExperience(Integer experience) {
		this.experience = experience;
	}

	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", firstName=" + firstName + ", lastName=" + lastName + ", ctc="
				+ ctc + ", experience=" + experience + "]";
	}

}
