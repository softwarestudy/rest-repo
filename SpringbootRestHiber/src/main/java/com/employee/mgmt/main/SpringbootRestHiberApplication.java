package com.employee.mgmt.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.employee"})
public class SpringbootRestHiberApplication {

	public static void main(String[] args) {
		//System.setProperty("server.servlet.context-path", "/SpringbootRestHiber");
		SpringApplication.run(SpringbootRestHiberApplication.class, args);
	}
}
